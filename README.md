# bib-purge - Purge unused entries from a .bib file

bib-purge is a very simple program which leverages the `bibtex-ruby` gem
to purge entries from a .bib file that are never cited by a given .tex
file.

## Installation

```
gem build bib-purge.gemspec
gem install bib-purge*.gemspec --user
```

## Usage

`bib-purge BIBFILE TEXFILE`

By default, it will print the .bib file without unnecessary entries to the
standard output. To perform in-place replacement, pass it the `-i` flag.

## License

Everything in this repository is released under the
GNU Affero General Public License, version 3 or any later version, unless
otherwise specified (and even in that case, it's all free software).
