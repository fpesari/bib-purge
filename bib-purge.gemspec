# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

Gem::Specification.new do |s|
  s.name          = 'bib-purge'
  s.version       = '0.1.2'
  s.summary       = 'Purge unused entries from a .bib file'
  s.description   = <<~DESC
    Purge entries from a BibTeX file never cited by a given LaTeX file.
  DESC
  s.authors       = ['Fabio Pesari']
  s.homepage      = 'https://gitlab.com/fpesari/bib-purge'
  s.files         = %w[.rubocop.yml bib-purge.gemspec LICENSE bin/bib-purge
                       README.md]
  s.executables   = %w[bib-purge]
  s.license       = 'AGPL-3.0+'
  s.add_runtime_dependency 'bibtex-ruby', '~>5.1', '>=5.1.5'
end
